﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using HtmlAgilityPack;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using SearchEngine.Models;
 
using static SearchEngine.Models.SearchEngine;

namespace SearchEngine.Controllers
{
    
    public class HomeController : Controller
    {
        private readonly IConfiguration _config;
        public bool cacheResponses = true;
        public Dictionary<string, Result[]> cache = new Dictionary<string, Result[]>();
        private const int MAX_RESULT = 100;
        private const int CACHE_MINS = 60;
        public HomeController(IConfiguration config)
        {
            _config = config;
        }
        public IActionResult Index()
        {

            BindSearchDropDown();
            
            string defaulturl = _config.GetSection("Search")["URL"];
            string defaultkeyword = _config.GetSection("Search")["Keyword"];

            Google google = new Google(defaulturl, defaultkeyword);
            string result = google.AutoSearch();
            google.Results = result;
          
            return View(google);
        }
        private void BindSearchDropDown()
        {
            List<SelectListItem> searchEngineType = new List<SelectListItem>()
            {
                new SelectListItem{ Text="Google", Value = "http://google.com.au" },
                new SelectListItem{ Text="Bing", Value = "http://bing.com.au" }
            };
           
            ViewData["SearchEngineType"] = searchEngineType;
        }
      
    }
}
