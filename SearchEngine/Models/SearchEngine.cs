﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SearchEngine.Models
{
    public class SearchEngine
    {
       
        //to bind list of records to dropdownlist  
        
            public string SearchEngineType { get; set; }
            public string URL { get; set; }
            public string Keyword { get; set; }

            public string results { get; set; } 
    }


}
