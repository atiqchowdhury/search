﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SearchEngine.Models
{
    public abstract class Search
    {
        public abstract string AutoSearch();
        public abstract void LoadCache();
        public abstract void SaveCache();

        public abstract Result[] GetPosition();
    }
}
