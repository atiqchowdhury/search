﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using HtmlAgilityPack;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using SearchEngine.Models;

namespace SearchEngine.Models
{
    public class Google : Search
    {
        private const int MAX_RESULT = 100;
        private const int CACHE_MINS = 60;
        private bool cacheResponses = true;
        private string searchEngineURL = "http://google.com.au";
        public Dictionary<string, Result[]> cache = new Dictionary<string, Result[]>();
        public string SearchEngineType { get; set; }
        public string URL { get; set; }
        public string Keyword { get; set; }

        public string Results { get; set; }

        public Google(string url, string keyword)
        {
             
            this.URL = url;
            this.Keyword = keyword;
        }

        public override string AutoSearch()
        {

            try
            {
                if (cacheResponses && cache.Count == 0)
                    LoadCache();
                Result[] results;
                results = GetPosition();
                string joined = string.Join(",", results.ToList().Select(x => x.position));
                return joined.Length > 0 ? joined : "0";

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public override Result[] GetPosition()
        {
            string searchEngine = searchEngineURL;
            Result[] results = null;
            string raw = searchEngine + "/search?num=" + MAX_RESULT + "&q={0}&btnG=Search";
            string search = string.Format(raw, System.Uri.EscapeDataString(Keyword));
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(search);
            if (cache.ContainsKey(URL)) //Check If This Query Has Already Been Sent
                return cache[URL]; //Return Directly From Cache

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                using (StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                {

                    string html = reader.ReadToEnd();
                    results = FindURLPosition(html, URL, searchEngineURL);

                }
            }
            if (cacheResponses)
            {
                cache[URL] = results.ToArray(); //Set The Cache
                SaveCache(); //Save The Cache
            }

            return results.ToArray();
        }
        public override void LoadCache()
        {
            if (System.IO.File.Exists("GoogleCache.json"))
            {
                DateTime modification = System.IO.File.GetCreationTime("GoogleCache");
                if ((DateTime.Now - modification).TotalMinutes < CACHE_MINS)
                {
                    cache = JsonConvert.DeserializeObject<Dictionary<string, Result[]>>(System.IO.File.ReadAllText("GoogleCache"));
                }
                else
                {
                    System.IO.File.Delete("GoogleCache");//delete the file after 60 mins
                }
            }
        }
        public override void SaveCache()
        {
            System.IO.File.WriteAllText("GoogleCache", JsonConvert.SerializeObject(cache));
        }

        private Result[] FindURLPosition(string html, string url, string searchEngineType)
        {
            Keyword result = new Keyword();
            var Webget = new HtmlWeb();
            var page = Webget.Load(searchEngineType);
            page.LoadHtml(html);
            List<Result> results = new List<Result>();
            var list = page.DocumentNode.SelectNodes("//div[contains(@class, 'kCrYT')]//a");//google has kCrYT as keyword for the url.  If it changes, then code need to be changed. 
            int count = list.Count();
            int i = 0;
            foreach (var obj in list)
            {
                i++;
                if (i > count)
                {
                    break;
                }
                else
                {
                    var urls = obj.SelectSingleNode(".").Attributes["href"].Value;
                    if (urls.Contains(url))
                    {

                        result.Postion = i;
                       
                        results.Add(new Result()
                        {
                            position = i.ToString()
                        });
                    }

                }
            }
            return results.ToArray();

        }
    }
}
