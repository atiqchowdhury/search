﻿using System;
using System.Collections.Generic; 
using System.IO;
using System.Linq;
using System.Net;
using System.Text; 
using HtmlAgilityPack; 
using Newtonsoft.Json;
 

namespace SearchEngine.Models
{
    public class Bing:Search
    {
        private const int MAX_RESULT = 100;
        private const int CACHE_MINS = 60;
        private bool cacheResponses = true;
        private string searchEngineURL = "http://bing.com.au";
        public Dictionary<string, Result[]> cache = new Dictionary<string, Result[]>();
        public string SearchEngineType { get; set; }
        public string URL { get; set; }
        public string Keyword { get; set; }

        public string Results { get; set; }
        public Bing(string url, string keyword)
        {

            this.URL = url;
            this.Keyword = keyword;
        }

        public override string AutoSearch()
        {
            throw new NotImplementedException();
        }

        public override void LoadCache()
        {
            throw new NotImplementedException();
        }

        public override void SaveCache()
        {
            throw new NotImplementedException();
        }

        public override Result[] GetPosition()
        {
            throw new NotImplementedException();
        }
    }
}
